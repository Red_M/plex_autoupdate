#!/usr/bin/env python2
import os
import sys
import requests
import xmltodict
import json
import string
import random
import urllib2
import subprocess

os.chdir('.' or sys.path[0])
current_dir = os.path.join(os.getcwd(),os.sep.join(sys.argv[0].split(os.sep)[0:-1]))
if current_dir.endswith('.'):
    current_dir = current_dir[0:-1]
if not sys.argv[0].split(os.sep)[-1] in os.listdir(current_dir):
    exit()


TOKEN = ''
SERVER_NAME = ''

SIGN_IN_URL = 'https://plex.tv/users/sign_in.json'
# SERVERS_URL = 'https://plex.tv/pms/servers.xml'
SERVERS_URL = 'http://127.0.0.1:32400/servers'
UPDATE_URL = 'https://plex.tv/downloads/latest/1?channel=4&build=linux-ubuntu-x86_64&distro=ubuntu&X-Plex-Token='
SESSIONS_URL = 'http://127.0.0.1:32400/status/sessions?X-Plex-Token='

def debugger(lvl=5,message=""):
    message = str(message)
    levels = {
        0:'FATAL',
        1:'CRITICAL',
        2:'ERROR',
        3:'INFO',
        4:'MESSAGE',
        5:'DEBUG'
    }
    if lvl in levels:
        lvl = levels[lvl]
    if "\n" in message:
        message = message.replace("\n","\n"+str(lvl)+": ")
    print(str(lvl)+": "+message)

def print_progress(count,total,fill='#',space='-'):
    rows, columns = os.popen('stty size', 'r').read().split()
    columns = int(columns)
    per = (float(count)/float(total))*100
    if not (per==None or per==''):
        per = str(round(per,2))+'% '
    else:
        per = ''
    progress_size = columns-(2+len(per))
    done = int(progress_size * count / total)
    sys.stdout.write('\r%s[%s%s]' % (per, fill[0] * done, space[0] * (progress_size-done)) )
    sys.stdout.flush()

def trace_back():
    type_, value_, traceback_ = sys.exc_info()
    ex = traceback.format_exception(type_, value_, traceback_)
    return('\n'.join(ex))

def wget(s,url,payload={},headers={},secure=True):
    payload2 = {
        "X-Plex-Platform":"Auto Plex Updater",
        "X-Plex-Device":"Auto Plex Updater",
        "X-Plex-Device-Name":"Auto Plex Updater",
        "X-Plex-Product":"Auto Plex Updater",
        "X-Plex-Version":"1.0",
        "X-Plex-Client-Identifier":"Auto-Plex-Updater-1.0"
    }
    if not payload=={}:
        payload2.update(payload)

    headers2 = {
        "X-Plex-Platform":"Auto Plex Updater",
        "X-Plex-Device":"Auto Plex Updater",
        "X-Plex-Device-Name":"Auto Plex Updater",
        "X-Plex-Product":"Auto Plex Updater",
        "X-Plex-Version":"1.0",
        "X-Plex-Client-Identifier":"Auto-Plex-Updater-1.0"
    }
    if not headers=={}:
        headers2.update(headers)
    if secure==False:
        r = s.get(url, params=payload2, headers=headers2,verify=secure)
    else:
        r = s.get(url, params=payload2, headers=headers2)
    # print(r.content)
    return(r.content)

def wpost(s,url,payload={},headers={}):
    headers2 = {
        "X-Plex-Platform":"Auto Plex Updater",
        "X-Plex-Device":"Auto Plex Updater",
        "X-Plex-Device-Name":"Auto Plex Updater",
        "X-Plex-Product":"Auto Plex Updater",
        "X-Plex-Version":"1.0",
        "X-Plex-Client-Identifier":"Auto-Plex-Updater-1.0"
    }
    if not headers=={}:
        headers2.update(headers)
    r = s.post(url, data=payload, headers=headers2)
    return(r.content)

def expand_url(url):
    fp = urllib2.urlopen(url)
    url = fp.geturl()
    return(url)

def jsonify(input):
    return(json.loads(input))

def xml_json(input):
    return(jsonify(json.dumps(xmltodict.parse(input))))

def plex_login(s,user,passwd):
    auth = {
        "user[login]":user,
        "user[password]":passwd
    }
    return(jsonify(wpost(s,SIGN_IN_URL,auth)))

def plex_get_sessions(req_s,token):
    url = SESSIONS_URL+token
    session_count = int(xml_json(wget(req_s,url,{'X-Plex-Token':token}))['MediaContainer']['@size'])
    return(session_count)

def get_deb(link,file_name):
    with open(file_name, "wb") as f:
        debugger(3,'Downloading Plex update, %s' % file_name)
        response = requests.get(link, stream=True)
        total_length = response.headers.get('content-length')

        if total_length is None: # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                try:
                    print_progress(dl,total_length)
                except Exception as e:
                    pass
            sys.stdout.write('\n')

def install_deb(file):
    try:
        out = subprocess.check_output(["dpkg", "-i", file])
    except Exception as e:
        out = trace_back()
    return(out)

def main():
    req_s = requests.Session()

    # login_data = plex_login(req_s,user,passwd)
    # token = login_data['user']['authentication_token']
    token = TOKEN

    servers = xml_json(wget(req_s,SERVERS_URL,{'X-Plex-Token':token}))
    dl_url = expand_url(UPDATE_URL+token)

    session_count = plex_get_sessions(req_s,token)
    if session_count==0:
        # debugger(3,'Checking for updates.')
        # print(servers)
        if 'Server' in servers['MediaContainer']:
            if isinstance(servers['MediaContainer']['Server'],type({})):
                servers['MediaContainer']['Server'] = [servers['MediaContainer']['Server']]
            for server in servers['MediaContainer']['Server']:
                if server['@name']==SERVER_NAME:
                    current_server_version = server['@version']
                    latest_version = dl_url.split('/')[-3]
                    deb_file_name = dl_url.split('/')[-1]

                    if not current_server_version==latest_version:
                        debugger(3,'Found update, updating Plex!')
                        deb_file_location = os.path.join(current_dir,deb_file_name)

                        get_deb(dl_url,deb_file_location)
                        debugger(3,'Download finished!')
                        debugger(3,'Installing update!')
                        debugger(3,install_deb(deb_file_location))
                        debugger(3,'https://forums.plex.tv/discussion/62832/plex-media-server')
                        debugger(3,'https://forums.plex.tv/categories/release-announcements')

if __name__=='__main__':
    main()
